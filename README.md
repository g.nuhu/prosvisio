# CI/CD Pipeline con GitLab ed Heroku

Lo scopo di questo lavoro è quello di costruire una pipeline di Continuous Integration / Continuous Deployment per un' applicazione a scelta.
L'applicazione scelta, seppur semplice, deve prevedere almeno due 'moduli' in modo che uno degli stage della pipeline possa essere quello di Integration Test.

1. **Scelta dell'applicazione**
    
    Per questo progetto si è scelta una semplice web app REST basata su NodeJs ed Express. L'applicazione si occuperà di svolgere operazioni CRUD su un' istanza in cloud del database MongoDB.
    Sul DB verranno inseriti, rimossi, aggiornati ipotetici studenti con nome, cognome e matricola.


2. **Predisposizione degli ambienti**

    - **Piattaforma di DevOps: GitLab**
        
        GitLab è un tool web-based di DevOps che fornisce un sistema di versioning Git e diverse funzionalità di CI/CD.
        GitLab è basato su una licenza open source e permette la creazione gratuita di repository sia pubblici che privati.
        La piattaforma fornisce molte funzionalità per automatizzare l'intero ciclo di vita di DevOps dalla pianificazione, sviluppo, build, verifica, testing, deploying e monitoring offrendo alta disponibilità e scalabilità.
        
        
        **Creazione di un nuovo progetto**
        
        Dopo essersi registrati il primo step da compiere è la creazione di un nuovo progetto (pubblico) su GitLab.
        
        In questo caso il nome del nuovo progetto è *2019_assignment1_CRUD_NodeJs*.
        
        Si procede andando nel tab Settings -> CI/CD e nella sezione *Variables* si impostano tre variabili che serviranno sostanzialmente a GitLab per effettuare la release/deploy su Heroku:
        
            - *HEROKU_API_KEY*: questa è la chiave segreta che si potrà trovare in seguito su Heroku e permetterà di effettuare il deploy dell'applicazione su un'istanza di un particolare account
            - *HEROKU_APP_PRODUCTION*: il nome dell'applicazione (su Heroku) in ambiente di produzione
            - *HEROKU_APP_STAGING*: il nome dell'applicazione (su Heroku) in ambiente di staging
            
        Per ora è tutto per quanto riguarda la configurazione di GitLab. 
        
        
    - **Ambiente Locale: NodeJs ed Express**
        
        Si procede clonando il repository del progetto GitLab in ambiente locale:
        
            - git clone https://gitlab.com/g.nuhu/2019_assignment1_crud_nodejs.git
            
        Fatto ciò occorre creare un nuovo branch, chiamato *Staging* in cui verrà eseguita la release dell'applicazione in ambiente appunto di staging:
        
            - git checkout -b Staging
            
        Per il deploy (in ambiente di produzione) verrà invece utilizzato il branch master, facendo il merge delle modifiche dal branch di staging qualora 
        l'applicazione si riveli stabile (abbia quindi passato le fasi di static analysis, unit test, integration test e user acceptance test) e sia quindi pronta 
        a raggiungere l'utente finale.
        
        
        Una volta installato NodeJs sul proprio pc occorre inizializzare il progetto come un'applicazione NodeJs, quindi:
            
            - cd C:/path_progetto
            - npm init
            
        Si procede ora installando il framework Express:
            
            - npm install -i express
            
        Dopodichè si installa l'applicazione:
        
            - npm install
            
        Si possono a questo punto pushare queste prime modifiche sul remote repository:
        
            - git add .
            - git commit -m "initial commit"
            - git push
            
        L'ultima cosa che rimane da fare per predisporre l'ambiente locale è quella di installare (e registrare) il *GitLab Runner*, ovvero quel componente che si occuperà di 'triggerare' 
        la pipeline di GitLab ogni qualvolta si pushino modifiche dal repo locale a quello remoto. 
        
        Per fare ciò ho seguito la documentazione ufficiale di GitLab: https://docs.gitlab.com/runner/install/windows.html
        
        
        
    - **Remote Server: Heroku**
    
        Heroku è una PaaS (Platform as a Service) che si occuperà sostanzialmente di 'ospitare' l'applicazione finale in cloud su un web server.

        Dopo aver creato un account Heroku, la prima cosa da fare è recuperare l' API KEY  in modo che GitLab possa collegarsi al nostro account Heroku per il deploy. 
        L'API KEY si trova nella sezione Accounts Settings -> Api Key. Una volta recuperata si va ad incollare nell'apposita sezione descritta in precedenza su GitLab.
        
        Si può quindi procedere creando una pipeline su Heroku che conterrà due *Apps*: Staging e Production, rispettamente per la release in ambiente di staging e per il deploy in produzione.
        Il nome delle due apps deve coincidere con quello specificato nelle Variables su GitLab.
        
    - **Database: MongoDB Atlas**
    
        MongoDB Atlas è un' piattaforma cloud del database non relazionale MongoDB.
        
        Dopo essersi registrati a MongoDB Atlas ciò che è sufficiente fare è creare un nuovo cluster, dopodichè accedendo alla scheda del cluster appena creato e navigando
        sul tab 'collections' è possibile creare un nuovo db per quel cluster.
        Cliccando poi sul tab 'overview' è possibile accedere al pulsante 'connect' che permette di recuperare la stringa di connessione che verrà utilizzata su Node per accedere all'istanza del DB.
        
        
        

3. **Sviluppo**

    *  Index.js
    
        Occorre a questo punto creare nella root del progetto (in locale) un file Index.js che conterrà gli endpoint che saranno chiamati per eseguire le operazioni CRUD sul DB.
        In particolare sono stati implementati i seguenti endpoint REST:
            - /     --> pagina Home
            
            - /people --> restituisce la lista degli studenti presenti nel db
            
            - /person --> inserisce uno studente sul db
            
            - /person/:code --> restituisce un determinato studente in base al numero di matricola
            
            - /removeAll --> rimuove tutti gli studenti dal db
            
            - /peopleSet --> inserisce un totale di 4 studenti sul db
            
        In questo file è necessario anche, per la connessione al db, specificare la stringa di connessione al db (ottenuta precedentemente su Atlas MongoDb) ed il nome della collection creata sul cluster:
        
            - const CONNECTION_URL = 'mongodb+srv://gnuhu:Pongoblu95@cluster0-wt0zd.mongodb.net/test?retryWrites=true&w=majority';
        
        
    * package.json
    
        Questo file è una sorta di manifesto per il progetto. Contiene diversi elementi come il nome del progetto NodeJs, la descrizione, tutte le dipendenze necessarie ed alcuni script associati ad alcuni comandi standard (start, test).
        


       
4. **CI/CD Pipeline**


    Avendo già configurato tutti gli ambienti coinvolti in questo progetto, ciò che è sufficiente fare per implentare una pipeline di Continous Delivery/Continuous Integration è creare e configurare un file
    chiamato `.gitlab-ci.yml`. Quando il gitLab Runner configurato in precedenza sarà triggerato da una push sul repository andrà a leggere questo file che contiene i job che la pipeline dovrà compiere e la loro descrizione formale.
    
    Il gitLab Runner è stato configurato in modo tale che ogni job utilizzi Docker, questo è possibile grazie ad un Docker Executor.
    
    Il Docker Executor si connette ad un Docker Engine ed esegue ogni build in un container separato ed isolato usando l'immagine docker predefinita nel file `.gitlab-ci.yml`.
    
    
    Si può quindi iniziare a definire alcuni elementi di tale file:
    
        image: node:latest
        
        before_script:
            - apt-get update -qy
            - apt-get install -y ruby-dev
            - gem install dpl
        
        stages:
            - build
            - verify
            - integrationTest
            - release
            - deploy
            
        cache:
            paths: 
                - node_modules/
                
    
    - image --> definisce la Docker image globale su cui i singoli job verranno eseguiti. La definizione di questa voce nei job specifici la sovrascrive.
    
    - before_script --> definisce alcuni comandi che devono essere eseguiti prima che ogni singolo job venga eseguito. In questo caso si ha un 'update' della package list, 
    l'installazione del pacchetto 'ruby' e l'installazione del tool 'dpl' (un tool per il deploy usato per il continuos deployment).
    
    - stage --> la lista dei job che la pipeline deve eseguire.
    
    - cache: --> definisce una path 'comune' fra tutti i job che non verrà sovrascritta all'avvio di ognuno.
        
    
    A questo punto è possibile definire i job specifici:
    
        
    **build**
        
        build:
            stage: build
            image: node:latest
            script:
                - npm install
            artifacts:
                paths:
                    - node_modules/
                
    Questo job non fa altro che eseguire la build (installazione del pacchetto con relative dipendenze e link di quest' ultime) verificando che venga portata a termine.
            
        
    **verify**
        
        verify:
            stage: verify
            image: node:latest
            script:
                - npm install eslint --save-dev
                - ./node_modules/.bin/eslint ./index.js > result.txt
            allow_failure: true
            artifacts:
                paths:
                    - ./result.txt
                when: on_failure
                
    Questo job si occupa di eseguire un processo di analisi statica del codice e di verificare che non contenga errori di sintassi.
    Come si può vedere dalla sezione 'script', viene infatti installato il tool `eslint` che è uno strumento di analisi statica del codice per JavaScript. 
    Dopodichè si esegue 'eslint' specificando che l'output venga generato nel file result.txt.
    Per scopi didattici si è inserito il comando `allow_failure: true` che permette di non killare la pipeline qualora l'analisi statica fallisca.
    In caso di fallimento viene quindi creato l'artefatto 'result.txt' nella root del progetto.
    
    
    Eslint esegue un' analisi statica del codice in base alle regole formali contenute nel file `.eslintrc.js` che occorre creare nella root del progetto (ovviamente prima di far partire la pipeline) e popolare con le regole desiderate (vedasi documentazione eslint: https://eslint.org/docs/rules/)
            
            
    **integrationTest**
        
    Questo job si occuperà, come indica il nome, di eseguire un integration test sull'applicazione. Un integration test è un test in cui si verifica che tutte le componenti di un sistema (software o hardware) comunichino e funzioni correttamente tra loro.
    In questo caso viene definito integration test perchè si verifica che il modulo software NodeJs comunichi correttamente con il DB MongoDB e che il funzionamento risulti corretto sia a livello funzionale che appunto tecnico.
            
        integrationTest:
            type: test
            stage: integrationTest
            image: node:latest
            script:
                - npm install -g chai
                - npm install -g mocha
                - npm run test
            
    Per questa fase di integration test si è scelto di utilizzare i framework di testing per Javascript **Mocha** e **Chai**.
    Vengono quindi installati entrambi i framework e subito dopo si ha il comando `run test` che altro non fa che eseguire ciò che è specificato in `package.json` 
    sotto la voce 'test', ovvero `mocha test.js`. 
    Viene quindi eseguito mocha sul file test.js, di seguito il suo contenuto:
            
        describe('Prosvisio Integration Test', function() {
    
        chai.use(chaihttp);
    	this.timeout(1000000);
    	
    	it('/ should return 200 status code', (done) => {
    	
    		chai.request(server)
    				.get('/')
    				.end((err, res) => {
    					testAsync(done, function(){
    						  expect(err).to.be.null;
    							expect(res).to.have.status(200);
    					}.bind(res));
    				});
        });
    	
    	it('/people should return status 200 and 4 people', (done) => {
    	
    		chai.request(server)
    				.get('/people')
    				.end((err, res) => {
    					testAsync(done, function(){
    						  expect(err).to.be.null;
    							expect(res).to.have.status(200);
    							expect(res).to.be.json;
    							
    							assert.equal(res.body.length, 4);
    					}.bind(res));
    				});
            });
        	
        
        
        });
            
    Come si può vedere vengono descritti due casi di test:
    
        - il path '/' deve restituire status code 200 (OK)
        
        - il path '/people' deve restituire status code 200 e 4 studenti dal db
        
    
    L'applicazione deve passare entrambi i casi di test per far sì che la pipeline prosegua.
            
            
            
    **release**
        
    Questo job effettua una release (ovvero una versione non definitiva del software, che non può quindi ancora raggiungere l'utente finale) dell'applicazione in ambiente di staging su Heroku.
                
        release:
            type: deploy
            image: ruby:latest
            stage: release
            script:
                - dpl --provider=heroku --app=$HEROKU_APP_STAGING --api-key=$HEROKU_API_KEY
            only:
                - staging
                        
    Il comando `dpl --provider=heroku --app=$HEROKU_APP_STAGING --api-key=$HEROKU_API_KEY` esegue il tool dpl che si occupa personalmente di effettuare la release su heroku, passandogli alcuni parametri:
            
        --provider: il servizio (in questo caso Heroku) su cui effettuare la release
        
        --app: su quale applicazione eseguire il deploy su Heroku (impostate nelle variabili su GitLab)
        
        --api-key: la chiave segreta dell'account Heroku su cui effettuare la release
                    
    Il comando `only: - staging`, invece, specifica che questo job deve essere eseguito solo quando il gitLab runner viene triggerato dal branch 'staging' del repository.
            
                
    **deploy**
            
    Questo job è esattamente identico a quello precedente con la differenza che viene eseguito un deploy in ambiente di produzione su Heroku.
    Il comando `only: - master`, infatti, specifica chr questo job deve essere eseguito solo quando il gitLab runner viene triggerato dal branch 'master'.
                
                
5. **Trigger della pipeline e verifica funzionamento**
            
        
    A questo punto per far partire la pipeline di CI/CD su GitLab è sufficiente eseguire un commit (dal branch 'staging') delle modifiche fatte finora sul repo locale su GitLab, quindi:

        - git add .
        
        - git commit -m "triggering pipeline"
        
        - git push 
        
    Dovrebbe quindi partire la pipeline eseguendo i job descritti in precedenza e concludendosi con una release in ambiente di staging su Heroku.
    
    Se questa fase dà esito positivo si può procedere effettuando un merge delle modifiche sul branch 'master' e triggerando quindi nuovamente la pipeline che eseguirà però questa volta un deploy in ambiente di produzione su Heroku:
    
        - git checkout master
        
        - git merge staging
        
        - git push
        
    
    Link all' app su Heroku: https://prosvisio-production.herokuapp.com/
