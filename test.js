var chai = require('chai'); 
var chaihttp = require('chai-http');
var server = 'https://prosvisio-production.herokuapp.com';
var expect = chai.expect;
var assert = chai.assert;

function testAsync(done, fn) {
    try {
        fn();
        done();
    } catch(err) {
        done(err);
    }
}

describe('Prosvisio Integration Test', function() {

    chai.use(chaihttp);
	this.timeout(1000000);
	
	it('/ should return 200 status code', (done) => {
	
		chai.request(server)
				.get('/')
				.end((err, res) => {
					testAsync(done, function(){
						  expect(err).to.be.null;
							expect(res).to.have.status(200);
					}.bind(res));
				});
    });
	
	it('/people should return status 200 and 4 people', (done) => {
	
		chai.request(server)
				.get('/people')
				.end((err, res) => {
					testAsync(done, function(){
						  expect(err).to.be.null;
							expect(res).to.have.status(200);
							expect(res).to.be.json;
							
							assert.equal(res.body.length, 4);
					}.bind(res));
				});
    });
	


});