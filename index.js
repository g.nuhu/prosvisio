const Express = require('express');
const BodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;

const CONNECTION_URL = 'mongodb+srv://gnuhu:Pongoblu95@cluster0-wt0zd.mongodb.net/test?retryWrites=true&w=majority';
const DATABASE_NAME = 'example';

var app = Express();

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));

var database, collection;

app.listen(process.env.PORT || 5000, () => {
	MongoClient.connect(CONNECTION_URL, { useNewUrlParser: true }, (error, client) => {
		if(error) {
			throw error;
		}
		database = client.db(DATABASE_NAME);
		collection = database.collection('people');
		console.log('Connected to `' + DATABASE_NAME + '`!');
	});
});

// set up route
app.get('/', (request, response) => {
	response.status(200).json({
		message: 'Welcome to prosvisio test application',
	});
});


app.post('/person', (request, response) => { //Insert one person
	collection.insert(request.body, (error, result) => {
		if(error) {
			return response.status(500).send(error);
		}
		response.send(result.result);
	});
});


app.get('/people', (request, response) => {
	collection.find({}).toArray((error, result) => {
		if(error) {
			return response.status(500).send(error);
		}
		response.send(result);
	});
});

app.get('/person/:code', (request, response) => {
	collection.findOne({ 'code': new ObjectId(request.params.id) }, (error, result) => {
		if(error) {
			return response.status(500).send(error);
		}
		response.send(result);
	});
});

app.get('/removeAll', (request, response) => {
	
	collection.deleteMany({ },  (error, result) => {
		if(error) {
			return response.status(500).send(error);
		}
		response.send(result.result);
		
	});
	
});

app.get('/peopleSet', (request, response) => {
	
	collection.insert([ {name: 'Grein', surname: 'Nuhu', code: '793022'},
		{name: 'Rossella', surname: 'Pandolfino', code: '859955'},
		{name: 'Michele', surname: 'Izzo', code: '745578'},
		{name: 'Roberta', surname: 'Lombardo', code: '678895'}], (error, result) => {
							
		if(error) {
			return response.status(500).send(error);
		}
		response.send(result);
	});
	

});


module.exports = app;
	
	
	
	
